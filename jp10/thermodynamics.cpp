#include "cantera/thermo.h"
#include <iostream>

using namespace Cantera;

void thermo_demo(const std::string& file, const std::string& phase)
{
  shared_ptr<ThermoPhase> gas(newPhase(file, phase)); // creates gas object at filename 'file' with phase 'phase'
  gas->setState_TPX(1500.0, 2.0*OneAtm, "O2:1.0, H2:3.0, AR:1.0");

  // temperature, pressure, density
  std::cout << gas->temperature() << std::endl;
  std::cout << gas->pressure() << std::endl;
  std::cout << gas->density() << std::endl;

  // molar thermo properties, _mass for specific thermo properties
  std::cout << gas->enthalpy_mole() << std::endl;
  std::cout << gas->entropy_mole() << std::endl;

  // chemical potentials of the species, can access each species
  int numSpecies = gas->nSpecies(); // how many species are there?
  printf("Number of species: %d", numSpecies);

  vector_fp mu(numSpecies); // vector_fp is a vector of doubles. we allocate to the vector 'mu' a number of entries specified by 'numSpecies'

  gas->getChemPotentials(&mu[0]);
  int n;

  printf("Below we will print the chemical potentials of all the species...");
  for (n = 0; n < numSpecies; n++)
  {
    std::cout << gas->speciesName(n) << " " << mu[n] << std::endl;
  }
}


int main(int argc, char** argv)
{
    try {
        thermo_demo("h2o2.cti","ohmech");
    } catch (CanteraError& err) {
        std::cout << err.what() << std::endl;
        return 1;
    }
    return 0;
}
