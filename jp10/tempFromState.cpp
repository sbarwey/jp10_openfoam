// For a given gas state, produce the temperature
// State is given by mass fraction, pressure, and total enthalpy


// order of species: H2, O2, H20, H, O, OH, HO2, H2O2, N2
#include <iostream>
#include <vector>
#include <string>
#include <cstring>
#include <sstream>
#include "cantera/thermo.h"
#include "cantera/kinetics.h"
#include "cantera/transport.h"

using namespace Cantera;
using namespace std;



string YToString(const vector<double>& Y)
{
  std::string str = "";
  for(int i = 0; i < Y.size(); i++)
  {
    std::ostringstream strs;
    strs << Y[i];
    switch (i)
    {
      case 0: str = "H2:" + strs.str() + ",";
        break;
      case 1: str = str + " O2:" + strs.str() + ",";
        break;
      case 2: str = str + " H2O:" + strs.str() + ",";
        break;
      case 3: str = str + " H:" + strs.str() + ",";
        break;
      case 4: str = str + " O:" + strs.str() + ",";
        break;
      case 5: str = str + " OH:" + strs.str() + ",";
        break;
      case 6: str = str + " HO2:" + strs.str() + ",";
        break;
      case 7: str = str + " H2O2:" + strs.str() + ",";
        break;
      case 8: str = str + " N2:" + strs.str();
        break;
    }
  }
  return str;
}

double getTempFromState(const vector<double>& Y, double P, double h_total)
{
  // h_total: specific enthalpy (J/kg)
  // P: pressure (Pa)
  // Y: vector of mass fractions corresponding to each species (dimensionless)

  // Convert the mass fractions to string format for cantera setState:
  std::string Y_str = YToString(Y);

  // Create new phase:
  std::unique_ptr<ThermoPhase> gas(newPhase("mueller.cti", "gas"));

  // Set the state with enthalpy and pressure:
  gas -> setState_HP(h_total, P);
  gas -> setMassFractionsByName(Y_str);
  std::cout << gas->report() << std::endl;
  return gas -> temperature();
}

void getReactionRate_k(const vector<double>& Y, double P, double h_total)
{
  // Convert the mass fractions to string format for cantera setState:
  std::string Y_str = YToString(Y);

  // Create new phase:
  std::unique_ptr<ThermoPhase> gas(newPhase("mueller.cti", "gas"));

  // Set the state with enthalpy and pressure:
  gas -> setState_HP(h_total, P);
  gas -> setMassFractionsByName(Y_str);

  // get reaction info

}

int main()
{
  vector<double> Y{0.3, 0.4, 0.3, 0, 0, 0, 0, 0, 0};
  double P = 500300.0;
  double h_total = 1.5e7;
  double T = getTempFromState(Y, P, h_total);
  printf("%f \n", T);
}
