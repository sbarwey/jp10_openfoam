// Make a homogeneous reactor and output the speceis source terms at each time step

#include "cantera/zerodim.h"
#include "cantera/thermo.h"
#include "cantera/kinetics.h"
#include "cantera/IdealGasMix.h"
#include "example_utils.h"

#include <iostream>

using namespace Cantera;

void react(const std::string& file, const std::string& phase)
{
  // Step 1: create a new phase
  IdealGasMix gas(file, phase);

  // Step 2: Set initial state of gas
  gas.setState_TPX(1100.0, 2*OneAtm, "H2:1, O2:1, N2:3.76"); // temp, press, mass frac
  // gas.equilibrate("HP");
  // gas.setState_TP(gas.temperature() - 600, gas.pressure());
  std::cout << gas.report() << std::endl;

  // Step 3: Create a reactor
  Reactor r; // homogenous reactor

  // Step 4: Insert gas into reactor
  r.insert(gas);

  // Step 5: Put reactor into sim
  ReactorNet sim;
  sim.addReactor(r);

  // Step 6
  int nsp = gas.nSpecies(); // number of species in mixture
  double dt = 1.e-6; // interval at which output is written
  int nsteps = 1000; // number of intervals

  // Step 7
  Array2D soln(nsp+4, 1); // 2d array to store QoIs
  saveSoln(0, 0.0, gas, soln); // helper function to save current state of gas

  // Step 8: main loop
  clock_t t0 = clock(); // save start time
  for (int i = 1; i <= nsteps; i++)
  {
      double tm = i*dt;
      sim.advance(tm);
      std::cout << "time = " << tm << " s" << std::endl;
      // std::cout << "temp = " << gas.temperature() << " s" << std::endl;
      saveSoln(tm, gas, soln); // updates soln with gas properties
  }
  clock_t t1 = clock(); // save end time

  // make a Tecplot data file and an Excel spreadsheet
  std::string plotTitle = "source term stuff: constant-pressure ignition";
  plotSoln("results.dat", "TEC", plotTitle, gas, soln);
  plotSoln("results.csv", "XL", plotTitle, gas, soln);
}

// using kinetics manager
void getSourceTerm()
{
  // create new phase
  std::unique_ptr<ThermoPhase> gas(newPhase("mueller.cti", "gas"));
  // List of phases participating in reactions (just one for homogeneous kinetics)
  std::vector<ThermoPhase*> phases{gas.get()};

  // Create the Kinetics object. Based on phase definition used, this will be a gaskinetics object
  std::unique_ptr<Kinetics> kin(newKineticsMgr(gas->xml(), phases));

  // Set an "interesting" mixture state where we will observe nonzero reaction rates.
  gas->setState_TPX(500.0, 2.0*OneAtm, "H2:1.0, O2:1.0, N2:3.76");
  gas->equilibrate("HP");
  gas->setState_TP(gas->temperature() - 100, gas->pressure());

  // Get the net reaction rates
  vector_fp wdot(kin->nReactions());
  printf("Number of reactions: %ld\n", kin->nReactions());
  kin->getNetRatesOfProgress(wdot.data());

  // Get the net production of species (source term)
  vector_fp source(kin->nTotalSpecies());
  printf("Number of species: %ld\n", kin->nTotalSpecies());
  kin->getNetProductionRates(source.data());

  // Get the creation rates
  vector_fp creation(kin->nTotalSpecies());
  kin->getCreationRates(creation.data());
  // Get the net destruction rates
  vector_fp destruction(kin->nTotalSpecies());
  kin->getDestructionRates(destruction.data());

  // Print
  writelog("Net reaction rates for all reactions ... \n");
  for (int i = 0; i < kin->nReactions(); i++)
  {
    writelog("{:3d}  {:30s}  {: .8e}\n", i, kin->reactionString(i), wdot[i]);
  }
  writelog("\n");

  // print species source terms :
  writelog("Source terms for all the species ... \n");
  for (int i = 0; i < kin->nTotalSpecies(); i++)
  {
    writelog("{:3s}   {: .8e}  {: .8e}  {: .8e}\n", kin->kineticsSpeciesName(i), creation[i],destruction[i],source[i]);
  }

}


int main(int argc, char** argv)
{
    // try {
    //     react("mueller.cti","gas");
    // } catch (CanteraError& err) {
    //     std::cout << err.what() << std::endl;
    //     return 1;
    // }

    try {
        getSourceTerm();
    } catch (CanteraError& err) {
        std::cout << err.what() << std::endl;
        return 1;
    }
    return 0;
}
