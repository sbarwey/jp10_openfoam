import cantera as ct
import numpy as np
import csv as _csv

#Solution controls:
maxTime=10           #max simulation time
deltaOutput=7.5       #output temp increment

#Initialize mechanism
gas=ct.Solution('chem.cti')

#Initial pressure temperature
Press = 1
T0 = 1500

#Initialize species related stuffs
ns = gas.n_species
Y = np.ones(ns)
Y[0:ns] = 0
nC2H4 = gas.species_index('C2H4')
nO2 = gas.species_index('O2')
nN2 = gas.species_index('N2')

#Stoichiometric factor f=Zst/(1-Zst), phi*f=Z/(1-Z)
f = 0.0637/(1-0.0637)
phi = 1.0
z = 1.0/(1.0+1.0/(f*phi))

#initialze massFrac
Y[0:ns] = 0
Y[nC2H4] = z*1.0
Y[nO2] = (1.0-z)*0.2331
Y[nN2] = (1.0-z)*0.7669

#outer loop is for initial temperature
deltaT = 25
T=T0-deltaT
while T<2501:
    #set TPY
    T = T+deltaT
    gas.TPY = T,100000*Press,Y

	#initialze homogenrous reactor
    r = ct.IdealGasConstPressureReactor(gas)
    sim = ct.ReactorNet([r])

    #inner loop is to perform time integration
    data=np.empty((0,3+ns), float)
    t=0.0
    Told=-999
    while t<maxTime:
        t=sim.step(1)
        if r.T>=Told+deltaOutput:
            print('{0:2.5f} {1:3.1f}'.format(t, r.T))
            data=np.append(data,[[t, r.T,r.thermo.enthalpy_mass]+list(r.thermo.Y)],axis=0)
            Told=r.T

    #Write data output
    filename='C2H4_p'+str(Press)+'phi'+str(int(phi*100))+'to'+str(T)+'.csv'
    csvfile = open(filename, 'w')
    writer = _csv.writer(csvfile)
    tmp=['t [ms]','T [k]','TotalEnthalpy [J/kg]']
    for n in range(0,ns):
        tmp = tmp+['Y-'+gas.species_names[n]]
    writer.writerow(tmp)
    for row in data:
        writer.writerow(row)
    csvfile.close()
