clear all
close all
clc

% run('~/Desktop/PROGRAMMING/MatlabScriptDesktop/startup.m');

S=importdata('Results.txt');
for i=1:length(S.colheaders)
    command=strcat(S.colheaders{i},'= S.data(:,i);');
    eval(command);
end

S=importdata('H2_p1phi100to1000.csv');
timeC=S.data(:,1);
TC=S.data(:,2);


figure(1)
hold on
h1=plot(Time,T,'Linewidth',5);
h2=plot(timeC,TC);
xlabel('Time [s]')
ylabel('T [K]')
legend([h1 h2], 'OF','Cant')
xlim([0 1e-3])
