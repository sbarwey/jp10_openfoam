clear all
close all
clc

run('~/Desktop/PROGRAMMING/MatlabScriptDesktop/startup.m');

S=importdata('Results.txt');
for i=1:length(S.colheaders)
    command=strcat(S.colheaders{i},'= S.data(:,i);');
    eval(command);
end

figure(1)
hold on
h1=plot(Time,H2);
h2=plot(Time,O2);
h3=plot(Time,H2O);
h4=plot(Time,H);
h5=plot(Time,O);
h6=plot(Time,OH);
h7=plot(Time,HO2);
h8=plot(Time,H2O2);
h9=plot(Time,N2);
xlabel('Time [s]')
legend([h1 h2 h3 h4 h5 h6 h7 h8 h9],'H2','O2','H2O','H','O','OH','HO2','H2O2','N2')

figure(2)
plot(Time,T,'+-');
xlabel('Time [s]')
ylabel('T [K]')
xlim([0 1e-2])

figure(3)
plot(Time,T,'+-');
xlabel('Time [s]')
ylabel('T [K]')
ylim([1000 1000.15])