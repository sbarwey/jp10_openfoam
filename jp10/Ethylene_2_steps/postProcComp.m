%clear all
%close all
%clc

%run('~/Desktop/PROGRAMMING/MatlabScriptDesktop/startup.m');

Prefix='or';
S=importdata('Results_red_1bar.txt');
for i=1:length(S.colheaders)
    command=[Prefix,strtrim(char(S.colheaders{i})),'= S.data(:,i);'];
    eval(command);
end

Prefix='tr';
S=importdata('Results_red_10bar.txt');
for i=1:length(S.colheaders)
    command=[Prefix,strtrim(char(S.colheaders{i})),'= S.data(:,i);'];
    eval(command);
end

Prefix='hr';
S=importdata('Results_red_100bar.txt');
for i=1:length(S.colheaders)
    command=[Prefix,strtrim(char(S.colheaders{i})),'= S.data(:,i);'];
    eval(command);
end

Prefix='od';
S=importdata('Results_det_1bar.txt');
for i=1:length(S.colheaders)
    command=[Prefix,strtrim(char(S.colheaders{i})),'= S.data(:,i);'];
    eval(command);
end

Prefix='td';
S=importdata('Results_det_10bar.txt');
for i=1:length(S.colheaders)
    command=[Prefix,strtrim(char(S.colheaders{i})),'= S.data(:,i);'];
    eval(command);
end

Prefix='hd';
S=importdata('Results_det_100bar.txt');
for i=1:length(S.colheaders)
    command=[Prefix,strtrim(char(S.colheaders{i})),'= S.data(:,i);'];
    eval(command);
end

Prefix='';
S=importdata('Results.txt');
for i=1:length(S.colheaders)
    command=[Prefix,strtrim(char(S.colheaders{i})),'= S.data(:,i);'];
    eval(command);
end

figure(1)
hold on
h1=plot(orTime,orT,'Linewidth',5);
h2=plot(trTime,trT,'Linewidth',5);
h3=plot(hrTime,hrT,'Linewidth',5);
h4=plot(odTime,odT,'-.','Linewidth',5);
h5=plot(tdTime,tdT,'-.','Linewidth',5);
h6=plot(hdTime,hdT,'-.','Linewidth',5);
plot(Time,T,'Linewidth',5);
xlabel('Time [s]')
ylabel('T [K]')
legend([h1 h2 h3 h4 h5 h6], '2-step, 1 bar','2-step, 10 bar','2-step, 100 bar', 'detailed, 1 bar', 'detailed, 10 bar', 'detailed, 100 bar')
xlim([1e-7 0.1]);
ylim([1000 4000]);
set(gca, 'XScale', 'log')

%timeRed=Time;
%TempRed=T;