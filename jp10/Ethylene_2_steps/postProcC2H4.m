clear all
close all
clc

run('~/Desktop/PROGRAMMING/MatlabScriptDesktop/startup.m');

S=importdata('Results.txt');
for i=1:length(S.colheaders)
    command=strcat(S.colheaders{i},'= S.data(:,i);');
    eval(command);
end

figure(1)
hold on
h1=plot(Time,C2H4);
h2=plot(Time,O2);
h3=plot(Time,CO);
h4=plot(Time,H);
h5=plot(Time,OH);
h6=plot(Time,CO2);
h7=plot(Time,H2O);
xlabel('Time [s]')
legend([h1 h2 h3 h4 h5 h6 h7],'C2H4','O2','CO','H','OH','CO2','H2O')

figure(2)
plot(Time,T,'+-');
xlabel('Time [s]')
ylabel('T [K]')
xlim([0 0.1])

