// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

// ~~~~ Headers for using Cantera

#include "cantera/IdealGasMix.h"
// ~~~~ Define the cantera namespace

using namespace Cantera;
// ~~~~ Define the basic properties

int nsp;								// number of species
std::vector<doublereal> mwt;			// molecular weights (kg/kmol)
std::vector<doublereal> hform;			// heat of formation (J/kg)

// ~~~ Define the state properties

doublereal temp;						// temperature (K)
doublereal engy;						// internal energy (J/kg)
doublereal dens;						// density (kg/m^3)
doublereal pres;						// pressure (Pa)

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

IdealGasMix initializeCantera( IOdictionary inputDict ){

	if ( Pstream::master() ){
		printf( "\n~~~~ Initializing Cantera ~~~~~~~~~~~~~~~~~~~~~~" );
		printf( "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" );
	}

	// ~~~~ Initialize the gas mixture

	IdealGasMix gas( canteraFile, canteraName );

	// ~~~~ Define the number of species

	nsp = static_cast<int>( gas.nSpecies() );

	// ~~~~ Assign the molecular weights (kg/kmol)

	mwt.reserve(nsp);
	gas.getMolecularWeights( mwt ); // (kg/kmol)

	// ~~~~ Assign the enthalpies of formation (J/kg)

	hform.reserve(nsp);
	for( int i=0; i<nsp; i++ ) hform[i] = gas.Hf298SS(i) / mwt[i];

	// ~~~~ Output to log

	if ( Pstream::master() ){
		cout << "\n";
		cout << "  canteraThermoFile : " << canteraFile << "\n";
		cout << "  canteraThermoName : " << canteraName << "\n";
		printf( "  number of species : %d\n", nsp );

		printf( "\n  | %12s---%20s---%19s | \n", "------------",
			"--------------------", "-------------------" );
		printf( "  | %12s | %20s | %19s | \n", "species name",
			"molec mass (kg/kmol)", "heat of form (J/kg)" );
		printf( "  | %12s | %20s | %19s | \n", "------------",
			"--------------------", "-------------------" );

		for( int i=0; i<nsp; i++ ) printf("  | %12s | %20.5f | %19.6e | \n",
			gas.speciesName(i).c_str(), mwt[i], hform[i] );
		printf( "  | %12s---%20s---%19s | \n", "------------",
			"--------------------", "-------------------" );

		printf( "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" );
		printf( "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" );
	}

	// ~~~~ Return

	return gas;
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

doublereal calculateTemp( IdealGasMix &gas, doublereal dens, doublereal engy,
	const doublereal* mass, doublereal tempGuess ){

	// ~~~~ Initialize the temperature

	doublereal temp = tempGuess;

	// ~~~~~ Calculate the error based on the initial guess

	gas.setState_TRY( temp, dens, mass );
	doublereal er = gas.intEnergy_mass() - engy;

	// ~~~~ Define iteration parameters

	int niter = 0;
	int iterFail = 0;
	double relaxPar = 1.0;

	// ~~~~ If the error is non-negligible, then check more temperatures

	if ( fabs(er/engy) >= 1e-10 ){

		// ~~~~ Calculate and check if the min temp encountered

		gas.setState_TRY( gas.minTemp(), dens, mass );
		if ( gas.intEnergy_mass() < engy ){

			// ~~~~ Calculate and check if the max temp encountered

			gas.setState_TRY( gas.maxTemp(), dens, mass );
			if ( gas.intEnergy_mass() > engy ){

				// ~~~~ Iterate to calculate the temperature

				while ( fabs(er/engy) >= 1e-12 ){

					// ~~~~ Update temperature and recalculate error

					gas.setState_TRY( temp+1.0, dens, mass );
					temp -= relaxPar * er/(gas.intEnergy_mass() - engy - er);
					gas.setState_TRY( temp, dens, mass );
					er = gas.intEnergy_mass() - engy;

					// ~~~~ Check iteration progress

					niter++;
					if ( niter > 20 and relaxPar == 1.0 ) {
					//	printf( "\n standard temp iteration stalled ==>" );
					//	printf( " moving to over-relaxed method" );
						relaxPar = 0.2;
					} else if ( niter > 100 ) {
					//	printf( "\n over-relaxed method failed (moving on)" );
					//	printf( "\n temperature       : %e", temp );
					//	printf( "\n calculated energy : %e", gas.intEnergy_mass() );
					//	printf( "\n expected energy   : %e", engy );
					//	printf( "\n relative error    : %e", fabs(er/engy) );
						iterFail = 1;
						break;
					}
				}

			// ~~~~ Set max temp as necessary

			} else {
				temp = gas.maxTemp();
				printf( "\n Maximum temperature encountered : %e %e %e \n",
					temp, engy, gas.intEnergy_mass() );
			}

		// ~~~~ Set min temp as necessary

		} else {
			temp = gas.minTemp();
			//printf( "\n Minimum temperature encountered : %e %e %e \n",
			//	temp, engy, gas.intEnergy_mass() );
		}
	}

	// ~~~~ If iteration failed, try direct method

	if ( iterFail == 1 ) {

		// ~~~~ Output to log file

		//printf( "\n Newton iteration for standard temp failed " );
		//printf( "==> trying direct method \n" );

		// ~~~~ Determine if approaching from below or from above

		double sgn = 1.0;
		if ( gas.intEnergy_mass() > engy ) sgn = -1.0;

		// ~~~~ Iterate to calculate the temperature

		double dtemp = 1.0;
		while ( dtemp > 1e-8 ) {

			// ~~~~ Update the temperature and check error

			temp += sgn*dtemp;
			gas.setState_TRY( temp, dens, mass );
			if ( sgn*(gas.intEnergy_mass() - engy) > 0 ){
				temp -= sgn*dtemp;
				dtemp *= 0.2;
			}

			// ~~~~ Output progress

			niter++;
			//printf( "standard temp iteration (niter/temp/dtemp/error) : %d %e %e %e \n",
			//	niter, temp, dtemp, gas.intEnergy_mass() - engy );
		}
	}

	// ~~~~ Return

	return temp;
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

doublereal calculateTemp_enth( IdealGasMix &gas, doublereal press, doublereal enth,
	const doublereal* mass, doublereal tempGuess ){

	// ~~~~ Initialize the temperature

	doublereal temp = tempGuess;

	// ~~~~~ Calculate the error based on the initial guess

	gas.setState_TPY( temp, press, mass);
	//gas.setState_TRY( temp, dens, mass );
	doublereal er = gas.enthalpy_mass() - enth;

	// ~~~~ Define iteration parameters

	int niter = 0;
	int iterFail = 0;
	double relaxPar = 1.0;

	// ~~~~ If the error is non-negligible, then check more temperatures

	if ( fabs(er/enth) >= 1e-10 ){

		// ~~~~ Calculate and check if the min temp encountered

		gas.setState_TPY( gas.minTemp(), press, mass );
		//gas.setState_TRY( gas.minTemp(), dens, mass );
		if ( gas.enthalpy_mass() < enth ){

			// ~~~~ Calculate and check if the max temp encountered

			gas.setState_TPY( gas.maxTemp(), press, mass );
			//gas.setState_TRY( gas.maxTemp(), dens, mass );
			if ( gas.enthalpy_mass() > enth ){

				// ~~~~ Iterate to calculate the temperature

				while ( fabs(er/enth) >= 1e-12 ){

					// ~~~~ Update temperature and recalculate error

					gas.setState_TPY( temp+1.0, press, mass );
					//gas.setState_TRY( temp+1.0, dens, mass );
					temp -= relaxPar * er/(gas.enthalpy_mass() - enth - er);
					gas.setState_TPY( temp, press, mass );
					//gas.setState_TRY( temp, dens, mass );
					er = gas.enthalpy_mass() - enth;

					// ~~~~ Check iteration progress

					niter++;
					if ( niter > 20 and relaxPar == 1.0 ) {
					//	printf( "\n standard temp iteration stalled ==>" );
					//	printf( " moving to over-relaxed method" );
						relaxPar = 0.2;
					} else if ( niter > 100 ) {
					//	printf( "\n over-relaxed method failed (moving on)" );
					//	printf( "\n temperature       : %e", temp );
					//	printf( "\n calculated energy : %e", gas.intEnergy_mass() );
					//	printf( "\n expected energy   : %e", engy );
					//	printf( "\n relative error    : %e", fabs(er/engy) );
						iterFail = 1;
						break;
					}
				}

			// ~~~~ Set max temp as necessary

			} else {
				temp = gas.maxTemp();
				printf( "\n Maximum temperature encountered : %e %e %e \n",
					temp, enth, gas.enthalpy_mass() );
			}

		// ~~~~ Set min temp as necessary

		} else {
			temp = gas.minTemp();
			//printf( "\n Minimum temperature encountered : %e %e %e \n",
			//	temp, engy, gas.intEnergy_mass() );
		}
	}

	// ~~~~ If iteration failed, try direct method

	if ( iterFail == 1 ) {

		// ~~~~ Output to log file

		//printf( "\n Newton iteration for standard temp failed " );
		//printf( "==> trying direct method \n" );

		// ~~~~ Determine if approaching from below or from above

		double sgn = 1.0;
		if ( gas.enthalpy_mass() > enth ) sgn = -1.0;

		// ~~~~ Iterate to calculate the temperature

		double dtemp = 1.0;
		while ( dtemp > 1e-8 ) {

			// ~~~~ Update the temperature and check error

			temp += sgn*dtemp;
			gas.setState_TPY( temp, press, mass );
			//gas.setState_TRY( temp, dens, mass );
			if ( sgn*(gas.enthalpy_mass() - enth) > 0 ){
				temp -= sgn*dtemp;
				dtemp *= 0.2;
			}

			// ~~~~ Output progress

			niter++;
			//printf( "standard temp iteration (niter/temp/dtemp/error) : %d %e %e %e \n",
			//	niter, temp, dtemp, gas.intEnergy_mass() - engy );
		}
	}

	// ~~~~ Return

	return temp;
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

doublereal calculateTotalTemp( IdealGasMix &gas, doublereal h0,
	const doublereal* mass, doublereal tempGuess ){

	// ~~~~ Initialize the temperature

	doublereal T0 = tempGuess;

	// ~~~~~ Calculate the error based on the initial guess
	//       Note that the enthalpy_mass() only based on temp/mass

	gas.setState_TRY( T0, 1.0, mass );
	doublereal er = gas.enthalpy_mass() - h0;

	// ~~~~ Define iteration parameters

	int niter = 0;
	int iterFail = 0;
	double relaxPar = 1.0;

	// ~~~~ If the error is non-negligible, then check more temperatures

	if ( fabs(er/h0) >= 1e-10 ){

		// ~~~~ Calculate and check if the min temp encountered

		gas.setState_TRY( gas.minTemp(), 1.0, mass );
		if ( gas.enthalpy_mass() < h0 ){

			// ~~~~ Calculate and check if the max temp encountered

			gas.setState_TRY( gas.maxTemp(), 1.0, mass );
			if ( gas.enthalpy_mass() > h0 ){

				// ~~~~ Iterate to calculate the temperature

				while ( fabs(er/h0) >= 1e-12 ){

					// ~~~~ Update temperature and recalculate error

					gas.setState_TRY( T0+1.0, 1.0, mass );
					T0 -= relaxPar * er/(gas.enthalpy_mass() - h0 - er);
					gas.setState_TRY( T0, 1.0, mass );
					er = gas.enthalpy_mass() - h0;

					// ~~~~ Check iteration progress

					niter++;
					if ( niter > 20 and relaxPar == 1.0 ) {
					//	printf( "\n total temp iteration stalled ==>" );
					//	printf( " moving to over-relaxed method" );
						relaxPar = 0.2;
					} else if ( niter > 100 ) {
					//	printf( "\n over-relaxed method failed (moving on)" );
					//	printf( "\n total temperature   : %e", T0 );
					//	printf( "\n calculated enthalpy : %e", gas.enthalpy_mass() );
					//	printf( "\n expected enthalpy   : %e", h0 );
					//	printf( "\n relative error      : %e", fabs(er/h0) );
						iterFail = 1;
						break;
					}
				}

			// ~~~~ Set max temp as necessary

			} else {
				T0 = gas.maxTemp();
				printf( "\n Maximum total temperature encountered : %e %e %e \n",
					T0, h0, gas.enthalpy_mass() );
			}

		// ~~~~ Set min temp as necessary

		} else {
			T0 = gas.minTemp();
			printf( "\n Minimum total temperature encountered : %e %e %e \n",
				T0, h0, gas.enthalpy_mass() );
		}
	}

	// ~~~~ If iteration failed, try direct method

	if ( iterFail == 1 ) {

		// ~~~~ Output to log file

		//printf( "\n Newton iteration for total temp failed " );
		//printf( "==> trying direct method \n" );

		// ~~~~ Determine if approaching from below or from above

		double sgn = 1.0;
		if ( gas.enthalpy_mass() > h0 ) sgn = -1.0;

		// ~~~~ Iterate to calculate the temperature

		double dtemp = 1.0;
		while ( dtemp > 1e-8 ) {

			// ~~~~ Update the temperature and check error

			T0 += sgn*dtemp;
			gas.setState_TRY( T0, 1.0, mass );
			if ( sgn*(gas.enthalpy_mass() - h0) > 0 ){
				T0 -= sgn*dtemp;
				dtemp *= 0.2;
			}

			// ~~~~ Output progress

			niter++;
		//	printf( "total temp iteration (niter/temp/dtemp/error) : %d %e %e %e \n",
		//		niter, T0, dtemp, gas.enthalpy_mass() - h0 );
		}
	}

	// ~~~~ Return

	return T0;
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

doublereal calculateStaticTemp( IdealGasMix &gas, doublereal h,
	const doublereal* mass, doublereal tempGuess ){

	// ~~~~ Initialize the temperature

	doublereal temp = tempGuess;

	// ~~~~~ Calculate the error based on the initial guess
	//       Note that the enthalpy_mass() only based on temp/mass

	gas.setState_TRY( temp, 1.0, mass );
	doublereal er = gas.enthalpy_mass() - h;

	// ~~~~ Define iteration parameters

	int niter = 0;
	int iterFail = 0;
	double relaxPar = 1.0;

	// ~~~~ If the error is non-negligible, then check more temperatures

	if ( fabs(er/h) >= 1e-10 ){

		// ~~~~ Calculate and check if the min temp encountered

		gas.setState_TRY( gas.minTemp(), 1.0, mass );
		if ( gas.enthalpy_mass() < h ){

			// ~~~~ Calculate and check if the max temp encountered

			gas.setState_TRY( gas.maxTemp(), 1.0, mass );
			if ( gas.enthalpy_mass() > h ){

				// ~~~~ Iterate to calculate the temperature

				while ( fabs(er/h) >= 1e-12 ){

					// ~~~~ Update temperature and recalculate error

					gas.setState_TRY( temp+1.0, 1.0, mass );
					temp -= relaxPar * er/(gas.enthalpy_mass() - h - er);
					gas.setState_TRY( temp, 1.0, mass );
					er = gas.enthalpy_mass() - h;

					// ~~~~ Check iteration progress

					niter++;
					if ( niter > 20 and relaxPar == 1.0 ) {
					//	printf( "\n static temp iteration stalled ==>" );
					//	printf( " moving to over-relaxed method" );
						relaxPar = 0.2;
					} else if ( niter > 100 ) {
					//	printf( "\n over-relaxed method failed (moving on)" );
					//	printf( "\n static temperature  : %e", temp );
					//	printf( "\n calculated enthalpy : %e", gas.enthalpy_mass() );
					//	printf( "\n expected enthalpy   : %e", h );
					//	printf( "\n relative error      : %e", fabs(er/h) );
						iterFail = 1;
						break;
					}
				}

			// ~~~~ Set max temp as necessary

			} else {
				temp = gas.maxTemp();
				printf( "\n Maximum static temperature encountered : %e %e %e \n",
					temp, h, gas.enthalpy_mass() );
			}

		// ~~~~ Set min temp as necessary

		} else {
			temp = gas.minTemp();
			printf( "\n Minimum static temperature encountered : %e %e %e \n",
				temp, h, gas.enthalpy_mass() );
		}
	}

	// ~~~~ If iteration failed, try direct method

	if ( iterFail == 1 ) {

		// ~~~~ Output to log file

	//	printf( "\n Newton iteration for static temp failed " );
	//	printf( "==> trying direct method \n" );

		// ~~~~ Determine if approaching from below or from above

		double sgn = 1.0;
		if ( gas.enthalpy_mass() > h ) sgn = -1.0;

		// ~~~~ Iterate to calculate the temperature

		double dtemp = 1.0;
		while ( dtemp > 1e-8 ) {

			// ~~~~ Update the temperature and check error

			temp += sgn*dtemp;
			gas.setState_TRY( temp, 1.0, mass );
			if ( sgn*(gas.enthalpy_mass() - h) > 0 ){
				temp -= sgn*dtemp;
				dtemp *= 0.2;
			}

			// ~~~~ Output progress

			niter++;
			//printf( "static temp iteration (niter/temp/dtemp/error) : %d %e %e %e \n",
			//	niter, temp, dtemp, gas.enthalpy_mass() - h );
		}
	}

	// ~~~~ Return

	return temp;
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

void speciesRR_redEth( IdealGasMix &gas, doublereal* wdot,
                doublereal T, doublereal press, doublereal* moleConc, doublereal* moleConcEqui, doublereal* mass,
                doublereal* fb, doublereal* fc, doublereal* fe,
                doublereal* ff, doublereal* fg ){


     doublereal RT = 8314.4621*T/1e6; // not sure if correct (SB: checks out, units in kJ/mol)
     doublereal A1 = 3.52e16;
     doublereal n1 = -0.70;
     doublereal E1 = 71.4;
     doublereal A2 = 2.60e19;
     doublereal n2 = -1.20;
     doublereal E2 = 0.0;
     doublereal A3 = 2.20e22;
     doublereal n3 = -2.0;
     doublereal E3 = 0.0;
     doublereal A4 = 2.18e23;
     doublereal n4 = -1.93;
     doublereal E4 = 499.0;
     doublereal A5 = 4.40e6;
     doublereal n5 = 1.50;
     doublereal E5 = -3.1;
     doublereal A6 = 4.97e8;
     doublereal n6 = 1.50;
     doublereal E6 = 89.7;
     doublereal A10 = 4.22e13;
     doublereal n10 = 0.0;
     doublereal E10 = 241.0;
     doublereal A11_0 = 1.90e35;
     doublereal n11_0 = -5.57;
     doublereal E11_0 = 21.1;
     doublereal A11_inf = 1.08e12;
     doublereal n11_inf = 0.45;
     doublereal E11_inf = 7.6;
     doublereal A12 = 2.00e12;
     doublereal n12 = 0.0;
     doublereal E12 = 20.9;
     doublereal A13_0 = 3.99e33;
     doublereal n13_0 = -4.99;
     doublereal E13_0 = 167.4;
     doublereal A13_inf = 1.11e10;
     doublereal n13_inf = 1.04;
     doublereal E13_inf = 153.8;

     // Reaction constants
     doublereal k1 = A1*std::pow(T,n1)*std::exp(-E1/RT);
     doublereal k2 = A2*std::pow(T,n2)*std::exp(-E2/RT);
     doublereal k3 = A3*std::pow(T,n3)*std::exp(-E3/RT);
     doublereal k4 = A4*std::pow(T,n4)*std::exp(-E4/RT);
     doublereal k10 = A10*std::pow(T,n10)*std::exp(-E10/RT);
     doublereal k11_0 = A11_0*std::pow(T,n11_0)*std::exp(-E11_0/RT);
     doublereal k11_inf = A11_inf*std::pow(T,n11_inf)*std::exp(-E11_inf/RT);
     doublereal k12 = A12*std::pow(T,n12)*std::exp(-E12/RT);
     doublereal k13_0 = A13_0*std::pow(T,n13_0)*std::exp(-E13_0/RT);
     doublereal k13_inf = A13_inf*std::pow(T,n13_inf)*std::exp(-E13_inf/RT);

     // Compute infinity variables
     gas.equilibrate("HP");
     doublereal Tinf = gas.temperature();
     doublereal RTinf = 8314.4621*Tinf/1e6;
	 //Info << "Teqn: " << Tinf << endl;
     gas.getConcentrations(moleConcEqui); // kmol/m3
     for (int is=0; is<nsp; is++) moleConcEqui[is] = moleConcEqui[is]*1.0e-3; // mol/cm3
     doublereal k3inf = A3*std::pow(Tinf,n3)*std::exp(-E3/RTinf);
     doublereal k4inf = A4*std::pow(Tinf,n4)*std::exp(-E4/RTinf);
     doublereal k5inf = A5*std::pow(Tinf,n5)*std::exp(-E5/RTinf);
     doublereal k6inf = A6*std::pow(Tinf,n6)*std::exp(-E6/RTinf);
     			//k11_inf = A11_inf*std::pow(Tinf,n11_inf)*std::exp(-E11_inf/RTinf);
     			//k13_inf = A13_inf*std::pow(Tinf,n13_inf)*std::exp(-E13_inf/RTinf);



     // Compute M
     doublereal Mb = 0.0;
     doublereal Mc = 0.0;
     doublereal Me = 0.0;
     doublereal Mf = 0.0;
     doublereal Mg = 0.0;
     //doublereal totalM = 0.0;
     for( int is=0; is<nsp; is++ ) {

     	Mb = Mb + fb[is]*moleConc[is];
     	Mc = Mc + fc[is]*moleConc[is];
     	Me = Me + fe[is]*moleConc[is];
     	Mf = Mf + ff[is]*moleConc[is];
     	Mg = Mg + fg[is]*moleConc[is];
        //totalM = totalM + moleConc[is];
     }
/*
Info << "Mb: " << Mb << endl;
Info << "Mc: " << Mc << endl;
Info << "Me: " << Me << endl;
Info << "Mf: " << Mf << endl;
Info << "Mg: " << Mg << endl;
Info << "totalM: " << totalM << endl;
*/
     // Troe's formula for reac eqn. 11

doublereal     Fc11 = 0.832 * std::exp(-T/1203.0);
doublereal     P_r11 = Mf * k11_0 /k11_inf;
doublereal     c_Troe11 = -0.4 - 0.67 * std::log10(Fc11);
doublereal     N_Troe11 = 0.75 - 1.27 * std::log10(Fc11);

doublereal     denominator_11 = std::log10(Fc11);
doublereal     nominator1_11 = std::log10(P_r11) + c_Troe11;
doublereal     nominator2_11 =  N_Troe11 - 0.14 * ( std::log10(P_r11) + c_Troe11 );

doublereal     log10_F11 = denominator_11 / ( 1 + std::pow(nominator1_11/nominator2_11,2) );
doublereal     k11 = k11_inf *  P_r11/(1 + P_r11) * std::pow(10,log10_F11 );

     // Troe's formula for reac eqn. 13

doublereal     Fc13 = 0.832 * std::exp(-T/1203.0);
doublereal     P_r13 = Mf * k13_0 /k13_inf;
doublereal     c_Troe13 = -0.4 - 0.67 * std::log10(Fc13);
doublereal     N_Troe13 = 0.75 - 1.27 * std::log10(Fc13);

doublereal     denominator_13 = std::log10(Fc13);
doublereal     nominator1_13 = std::log10(P_r13) + c_Troe13;
doublereal     nominator2_13 =  N_Troe13 - 0.14 * ( std::log10(P_r13) + c_Troe13 );

doublereal     log10_F13 = denominator_13 / ( 1 + std::pow(nominator1_13/nominator2_13,2) );
doublereal     k13 = k13_inf *  P_r13/(1 + P_r13) * std::pow(10,log10_F13 );

     // Compute auxiliary variables
     doublereal s = k3inf*moleConcEqui[4]/k4inf;
     doublereal r = moleConcEqui[1]/moleConcEqui[3];
     doublereal a = 2.0/(1.0 + k5inf/(r*k6inf));
     doublereal b = 2.0*(1.0+r)/(1.0+r+2.0*std::sqrt(r*s));
     doublereal c = (2.0-a)/2.0 + b*(1.0-r)/(2.0*(1+r));
     //doublereal t = k12*moleConc[2]/(k12*moleConc[2] + k13*Mf);
     doublereal t = k12*moleConc[2]/(k12*moleConc[2] + k13);
     //doublereal q = k1/(k1 + k2*Mb + t*k11);
     doublereal q = k1/(k1 + k2*Mb + t*k11*Mf);

     // Species order N2, H, O2, OH, H2O, CO, C2H4, CO2
	doublereal omega1 = 1.0;

Info << "s: " << s<< endl;
Info << "r: " << r<< endl;
Info << "a: " << a<< endl;
Info << "b: " << b<< endl;
Info << "c: " << c<< endl;
Info << "t: " << t<< endl;
Info << "q: " << q<< endl;
Info << "k3: " << k3 << endl;
Info << "k3inf: " << k3inf << endl;
Info << "k4: " << k4 << endl;
Info << "k4inf: " << k4inf << endl;
Info << "k5inf: " << k5inf << endl;
Info << "k6inf: " << k6inf << endl;
Info << "k10: " << k10 << endl;
Info << "k11: " << k11 << endl;
Info << "k11inf: " << k11_inf << endl;
Info << "k13: " << k13 << endl;
Info << "k13inf: " << k13_inf << endl;
// Heviside function H{ [C2H4] }
if (mass[6] <  1.0 && mass[6] > 5e-4 ){
     omega1 = k10*moleConc[6]*moleConc[2] + q*k1*moleConc[1]*moleConc[2]; // mol/(cm3.s)
}
else {
     omega1 = k10*moleConc[6]*moleConc[2]; // mol/(cm3.s)
	 Info << "Trigger HeaviSide Function" << endl;
}
     doublereal omega2 = (3.0/2.0)*(k3*Mc*moleConc[1]*moleConc[3] -
			 k4*Mc*moleConc[4]); // mol/(cm3.s)

// Unit convert to kmol/(m3.s)
     omega1 = omega1 * 1e3;
     omega2 = omega2 * 1e3;
	 omega1=std::max(omega1,0.0);
	 omega2=std::max(omega2,0.0);


Info << "massTmp:" << endl;
Info << " [H]: " << mass[1]<< endl;
Info << "[OH]: " << mass[3]<< endl;
Info << "[H2O]: " << mass[4]<< endl;
Info << "[C2H4]: " << mass[6]<< endl;
Info << "omega1: " << omega1 << endl;
Info << "omega2: " << omega2 << endl;
//Info << "RT: " << RT << endl;

     wdot[0]=0.0;
     wdot[1]=(2.0/3.0)*omega1 - (2.0/3.0)*omega2 + 2.0*b*r*omega2/(1.0+r);
     wdot[2]=-2.0*omega1 - c*omega2;
     wdot[3]=(2.0/3.0)*omega1 - (2.0/3.0)*omega2 + 2.0*b*omega2/(1.0+r);
     wdot[4]=(4.0/3.0)*omega1 - (4.0/3.0)*omega2 + (2.0-b)*omega2;
     wdot[5]=2.0*omega1 - 2.0*omega2 + a*omega2;
     wdot[6]=-omega1;
     wdot[7]=(2.0-a)*omega2;

//Info << "rho: " << gas.density() << endl;
     // Put the gas back in initial state Not sure if needed
     gas.setState_TPY(T, press, mass);

//Info << "gas.RT(): " << gas.RT() << endl;
//Info << "rho2:  " << gas.density() << endl;
}

void speciesRR_redJP10( IdealGasMix &gas, doublereal* wdot,
                doublereal T, doublereal press, doublereal* moleConc, doublereal* moleConcEqui, doublereal* mass, doublereal* fc)
{
	// Rate parameters:
	doublereal RT = 8314.4621*T/1e6; // not sure if correct (SB: checks out, units in kJ/mol)

	// 3rd elementary reaction: H + OH + M -> HO2 + M
	doublereal A3 = 2.20e22; // Note: reaction includes [M]
	doublereal n3 = -2.00;
	doublereal E3 = 0.0;

	// 4th elementary reaction: H2O + M -> H + OH + M
	doublereal A4 = 2.18e23; // Note: reaction includes [M]
	doublereal n4 = -1.93;
	doublereal E4 = 499.0;

	// 5th elementary reaction: CO + OH -> CO2 + H
	doublereal A5 = 4.40e6;
	doublereal n5 = 1.50;
	doublereal E5 = -3.1;

	// 6th elementary reaction: CO2 + H -> CO + OH
	doublereal A6 = 4.97e8;
	doublereal n6 = 1.50;
	doublereal E6 = 89.7;

	// 19th elementary reaction: JP10 + O2 -> HO2 + C3H5 + C2H2 + C5H8
	doublereal A19 = 7.92e13;
	doublereal n19 = 0.00;
	doublereal E19 = 199.21;

	// Rate constants based on Modified Arrhenius Law:
	doublereal  k3 = A3 *std::pow(T,n3)* std::exp(-E3/RT);
	doublereal  k4 = A4 *std::pow(T,n4)* std::exp(-E4/RT);
	doublereal  k5 = A5 *std::pow(T,n5)* std::exp(-E5/RT);
	doublereal  k6 = A6 *std::pow(T,n6)* std::exp(-E6/RT);
	doublereal k19 = A19*std::pow(T,n19)*std::exp(-E19/RT);

	// Compute 'infinity variables': first equilibrate the gas, then proceed normally
	gas.equilibrate("HP");
	doublereal Tinf = gas.temperature(); // (K)
	doublereal RTinf = 8314.4621*Tinf/1e6; // (kJ/mol)
	gas.getConcentrations(moleConcEqui); // (kmol/m3)

	// convert concentration to mol/cm3:
	for (int is=0; is<nsp; is++) moleConcEqui[is] = moleConcEqui[is]*1.0e-3; // (mol/cm3)

	doublereal k3inf = A3*std::pow(Tinf,n3)*std::exp(-E3/RTinf);
	doublereal k4inf = A4*std::pow(Tinf,n4)*std::exp(-E4/RTinf);
	doublereal k5inf = A5*std::pow(Tinf,n5)*std::exp(-E5/RTinf);
	doublereal k6inf = A6*std::pow(Tinf,n6)*std::exp(-E6/RTinf);


	// Reduced mechanism coefficients:
	// For jp10, we need 'a', 'b', 'c', 'r', 's'
	// species order (as per cti file): 0: N2, 1: H, 2: O2, 3: OH, 4: H2O, 5: CO, 6: JP10, 7: CO2
	int m = 10;
	int n = 16;
	doublereal f = 5/3;
	doublereal r = moleConcEqui[1]/moleConcEqui[3];
	doublereal s = k3inf * moleConcEqui[4]/k4inf;
	doublereal a = m/(1 + k5inf/ (r*k6inf));
	doublereal b = n*(1+r)/(2*(1 + r + 2 * std::sqrt(r*s)));
	doubleread c = ((m - a)/2) + b*(1 - r)/(2*(1 + r));

	// Print as an update:
	Info << "s: " << s<< endl;
	Info << "r: " << r<< endl;
	Info << "a: " << a<< endl;
	Info << "b: " << b<< endl;
	Info << "c: " << c<< endl;
	Info << "k3: " << k3 << endl;
	Info << "k3inf: " << k3inf << endl;
	Info << "k4: " << k4 << endl;
	Info << "k4inf: " << k4inf << endl;
	Info << "k5: " << k5 << endl;
	Info << "k5inf: " << k5inf << endl;
	Info << "k6: " << k6 << endl;
	Info << "k6inf: " << k6inf << endl;
	Info << "k19: " << k19 << endl;

	// Evaluate third-body concentration: only need chaperon efficiencies for footnote 'c'
	doublereal Mc = 0.0;
	// fc[0]=1.0; // N2
	// fc[1]=1.0; // H [SB: Check this]
	// fc[2]=1.0; // O2
	// fc[3]=1.0; // OH
	// fc[4]=12.0; // H20
	// fc[5]=1.9; // CO
	// fc[6]=1.0; // JP10
	// fc[7]=3.8; // CO2

	for(int is = 0; is < nsp; is++)
	{
		Mc = Mc + fc[is]*moleConc[is];
	}

	// Get reaction rates for each step: omega1 and omega2 
	doublereal omega1 = 50 * k19 * moleConc[6] * moleConc[2];
	doublereal omega2 = (k3 * Mc * moleConc[1] * moleConc[3] - k4 * Mc * moleConc[4])/f;

	// Unit convert to kmol/(m3.s)
	omega1 = omega1 * 1e3;
	omega2 = omega2 * 1e3;
	omega1=std::max(omega1,0.0); // hard check: omegas cannot be negative
	omega2=std::max(omega2,0.0);

	// Print an update:
	Info << "massTmp:" << endl;
	Info << "[H]: " << mass[1]<< endl;
	Info << "[OH]: " << mass[3]<< endl;
	Info << "[H2O]: " << mass[4]<< endl;
	Info << "[JP10]: " << mass[6]<< endl;
	Info << "omega1: " << omega1 << endl;
	Info << "omega2: " << omega2 << endl;

	// Get species net production rates:
	// N2:
	wdot[0] = 0.0;
	// H:
	wdot[1] = f*omega1 - f*omega2 + ((2.0*b*r)/(1.0+r))*omega2;
	// O2:
	wdot[2] = -((2*m + n)/4.0)*omega1 - c*omega2;
	// OH:
	wdot[3] = f*omega1 + ((2*b)/(1+r))*omega2;
	// H2O:
	wdot[4] = (n/2.0 - f)*(omega1 - omega2) + (n/2.0 - b)*omega2;
	// CO:
	wdot[5] = m*(omega1 - omega2) + a*omega2;
	// JP10:
	wdot[6] = -omega1;
	// CO2:
	wdot[7] = (m - a)*omega2;


	// Put the gas back in initial state (Not sure if needed):
	gas.setState_TPY(T, press, mass);
}
