#include "cantera/thermo.h"
#include <iostream>

using namespace Cantera;

void equil_demo()
{
    std::unique_ptr<ThermoPhase> gas(newPhase("h2o2.cti","ohmech"));
    gas->setState_TPX(1500.0, 2.0*OneAtm, "O2:1.0, H2:3.0, AR:1.0");
    gas->equilibrate("TP");
    std::cout << gas->report() << std::endl;
}

int main()
{
    try {
        equil_demo();
    } catch (CanteraError& err) {
        std::cout << err.what() << std::endl;
    }
}

// g++ -o equil -pthread -O3 -std=c++0x -I/Users/sbarwey/Files/cantera/include -L/Users/sbarwey/Files/cantera/build/lib -lcantera -L/Users/sbarwey/Files/sundials/instdir/lib -lsundials_cvodes -lsundials_ida -lsundials_nvecserial -llapack -lblas equil.cpp
