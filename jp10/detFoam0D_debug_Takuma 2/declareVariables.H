doublereal Ru = 8.3144598;
doublereal mass[nsp];
doublereal moleConc[nsp];
doublereal moleConcEqui[nsp];
doublereal wdot[nsp];
doublereal rhs_mass[nsp];
doublereal rho;
doublereal T;
doublereal P;
doublereal e;
doublereal massSave[nsp];
doublereal massTmp[nsp];
doublereal rhoSave;
doublereal TSave;
doublereal PSave;

doublereal mwt_mix;
doublereal sumMassFraction;
