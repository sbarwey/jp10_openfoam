// Set the initial conditions for the specied mass fraction
for( int i=0; i<nsp; i++ ) { 
        Info << gas.speciesName(i) << endl;
	if(gas.speciesName(i) == "C2H4") {
	        mass[i] = Y_C2H4;
	}
	else if(gas.speciesName(i) == "O2") {
	        mass[i] = Y_O2;
	}
	else if(gas.speciesName(i) == "CO") {
	        mass[i] = Y_CO;
	}
	else if(gas.speciesName(i) == "CO2") {
	        mass[i] = Y_CO2;
	}
	else if(gas.speciesName(i) == "H") {
	        mass[i] = Y_H;
	}
	else if(gas.speciesName(i) == "OH") {
	        mass[i] = Y_OH;
	}
	else if(gas.speciesName(i) == "H2O") {
	        mass[i] = Y_H2O;
	}
	else if(gas.speciesName(i) == "N2") {
	        mass[i] = Y_N2;
	}
}

bool flagIndivMassFraction = 0;
bool flagSumMassFraction = 0;
sumMassFraction=0.0;
// Verify that the initial conditions make sense
for( int i=0; i<nsp; i++ ) {

	sumMassFraction = sumMassFraction + mass[i];
	if(mass[i]>1.0 || mass[i]<0.0) {
		flagIndivMassFraction = 1;
		Info << "Mass fraction " << gas.speciesName(i) << "not bounded by 0 and 1" << endl;
	}

}


if(std::abs(sumMassFraction-1.0)>1e-10) {

	flagSumMassFraction = 1;
	Info << "Mass fraction sum =  " << sumMassFraction << " is too far from 1" << endl;

} 

if( flagIndivMassFraction || flagSumMassFraction) {

	FatalErrorInFunction << "Initial mass fraction values do not make sense"<< exit(FatalError);

}

// Rescale the mass fractions so their sum adds up to 1
Info << endl;
Info << "~~~~" << "Rescaled Mass fraction " << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ "<< endl; 
for( int i=0; i<nsp; i++ ) {

        mass[i] = mass[i]/sumMassFraction;
        Info << gas.speciesName(i) << " = " << mass[i] << endl;
}

// Get the mean molecular weight
gas.setState_TRY( 300, 1, mass ); // First two are dummies
mwt_mix = gas.meanMolecularWeight();

// Compute thermodynamic variables
P = press_0; 
T = temp_0; 
rho = P*1e5/ (T*8314.4621/mwt_mix);
gas.setState_TRY( T, rho, mass ); // First two are dummies

