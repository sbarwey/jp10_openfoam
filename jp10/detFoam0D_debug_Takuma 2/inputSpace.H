// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

// ~~~~ Cantera parameters

string canteraFile;
string canteraName;
int nSpecies_inpt;

// ~~~~ Matlab output parameters

int matlabDataFiles;

// ~~~~ Initial conditions

scalar Y_C2H4;
scalar Y_O2;
scalar Y_CO;
scalar Y_CO2;
scalar Y_H;
scalar Y_OH;
scalar Y_H2O;
scalar Y_N2;
scalar temp_0;      // Temperature
scalar press_0;     // Thermo pressure
scalar dens_0;

// ~~~~ Integration details

scalar totalDeltaT;  //Total intgeration time
word reacTimeMethod; //Integration Scheme


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

void initializeInput( IOdictionary inputDict ){

	if ( Pstream::master() ){ 
		printf( "\n~~~~ Initializing Input Dictionary ~~~~~~~~~~~~~" );
		printf( "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" );
	}

	// ~~~~ Read in Cantera parameters

	canteraFile = static_cast<string>( inputDict.lookup("canteraFile") );
	canteraName = static_cast<string>( inputDict.lookup("canteraName") );

	// ~~~~ Read in Matlab output parameters

	matlabDataFiles = readInt( inputDict.lookup("matlabDataFiles") );

	// ~~~~ Read in reactive (ODE) solver parameters

         
        totalDeltaT = readScalar( inputDict.lookup("TotalTime") );
	reacTimeMethod = static_cast<word>( inputDict.lookup("reacTimeMethod") );


        // ~~~~ Read Initial Specices mass fraction
          
        Y_C2H4 = readScalar( inputDict.lookup("Y_C2H4") );
        Y_O2 = readScalar( inputDict.lookup("Y_O2") );
        Y_CO = readScalar( inputDict.lookup("Y_CO") );
        Y_CO2 = readScalar( inputDict.lookup("Y_CO2") );
        Y_H = readScalar( inputDict.lookup("Y_H") );
        Y_OH = readScalar( inputDict.lookup("Y_OH") );
        Y_H2O = readScalar( inputDict.lookup("Y_H2O") );
        Y_N2 = readScalar( inputDict.lookup("Y_N2") );
        temp_0 = readScalar( inputDict.lookup("T(K)") );
        press_0 = readScalar( inputDict.lookup("P(atm)") );
           
  

	// ~~~~ Output to log

	if ( Pstream::master() ){
		cout << "\n";
		cout << "  Cantera Parameters \n";
		cout << "  - canteraFile     : " << canteraFile << "\n";
		cout << "  - canteraName     : " << canteraName << "\n";
		cout << "\n";
		cout << "  Matlab Output Parameters \n";
		cout << "  - matlabDataFiles : " << matlabDataFiles << "\n";
		cout << "\n";
		cout << "  Reactive (ODE) Solver Parameters \n";
		cout << "  - reacTimeMethod  : " << reacTimeMethod << "\n";
		cout << "\n";
                cout << "  Initial conditions \n";
                cout << "  - T  : " << temp_0 << "\n";
                cout << "  - P : " << press_0 << "\n";
                cout << "\n";
		printf( "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" );
		printf( "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" );
	}

	// ~~~~ Return

	return;
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
