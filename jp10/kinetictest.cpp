#include "cantera/thermo.h"
#include "cantera/kinetics.h"
#include "cantera/transport.h"

using namespace Cantera;

void simple_demo2()
{
  // Create a new phase
  std::unique_ptr<ThermoPhase> gas(newPhase("gri30.cti", "gri30_mix"));

  // List of phases participating in reactions (just one for homogeneous kinetics)
  std::vector<ThermoPhase*> phases{gas.get()};

  // Create the Kinetics object. Based on phase definition used, this will be a gaskinetics object
  std::unique_ptr<Kinetics> kin(newKineticsMgr(gas->xml(), phases));

  // Set an "interesting" mixture state where we will observe nonzero reaction rates.

  gas->setState_TPX(500.0, 2.0*OneAtm, "CH4:1.0, O2:1.0, N2:3.76");
  gas->equilibrate("HP");
  gas->setState_TP(gas->temperature() - 100, gas->pressure());

  // Get the net reaction rates
  vector_fp wdot(kin->nReactions());
  kin->getNetRatesOfProgress(wdot.data());

  writelog("Net reaction rates for reactions involving CO2\n");
  size_t kCO2 = gas->speciesIndex("CO2");
  for (size_t i = 0; i < kin->nReactions(); i++)
  {
    if (kin->reactantStoichCoeff(kCO2, i) || kin->productStoichCoeff(kCO2, i))
    {
      writelog("{:3d}  {:30s}  {: .8e}\n", i, kin->reactionString(i), wdot[i]);
    }
  }
  writelog("\n");
}

// the main program just calls function simple_demo2 within a 'try' block, and
// catches exceptions that might be thrown
int main()
{
    try {
        simple_demo2();
    } catch (std::exception& err) {
        std::cout << err.what() << std::endl;
    }
}


// g++ -o kinetics -pthread -O3 -std=c++0x -I/Users/sbarwey/Files/cantera/include -L/Users/sbarwey/Files/cantera/build/lib -lcantera -L/Users/sbarwey/Files/sundials/instdir/lib -lsundials_cvodes -lsundials_ida -lsundials_nvecserial -llapack -lblas kinetictest.cpp
